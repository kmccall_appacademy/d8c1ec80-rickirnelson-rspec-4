class Book
  attr_reader :title
  LOWERCASE_WORDS = ["a", "an", "the", "in", "of", "and"]

  def title=(title)
    @title=title.split(' ').map! {|word| LOWERCASE_WORDS.include?(word) ? word : word.capitalize}.join(' ')
    @title[0] = @title[0].upcase
    @title
  end
end
